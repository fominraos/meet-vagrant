# Что нужно сделать
## Установка необходимых приложений

- Установите [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
- Установите [Vagrant](https://www.vagrantup.com/downloads).
- Убедитесь, что установлен [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html). 


## Запуск приложения из репозитория при помощи Vagrant

- Возьмите код из репозитория `git clone https://gitlab.com/fominraos/meet-vagrant.git`.
- Запустите `vagrant up`.
- Проверьте, что сайт открывается в браузере по адресу http://10.10.10.10.


## Запуск приложения из локальной папки

Так как Vagrant используется для работы с локальным окружением для удобства разработки, скачайте [наш репозиторий](https://gitlab.com/fominraos/reactjs-deploy) в соседнюю директорию, что и репозиторий с Vagrant, и работайте уже с ним.
Далее в Ansible playbook закомментируйте следующие таски:       
- установка `nginx`, 
- клонирование репозитория,
- запуск `yarn build`,
- создание симлинка,
- установка npm-пакетов. 
- Удалите созданную виртуалку `vagrant destroy`.
- Добавьте в файл Vargant строку, которая позволит пробросить папку с приложением в виртуальную машину.
```
config.vm.synced_folder "../skillbox-deploy-blue-green/", "/var/www/releases/local", create: true
```
- Снова поднимите Vagrant `vagrant up`.
- Затем подключитесь к созданной виртуальной машине vagrant ssh.
- Зайдите в папку, в которой теперь хранится ваше приложение "cd /var/www/releases/local".
- Запустите yarn start.
- После старта сервера попробуйте в браузере перейти на http://10.10.10.10:3000/.
- Удалите созданную виртуалку `vagrant destroy`.
